FROM ubuntu:18.04
# RUN make /app
CMD mkdir -p ~/bin
CMD wget 'https://storage.googleapis.com/git-repo-downloads/repo' -P ~/bin
CMD chmod +x ~/bin/repo
CMD apt-get update
CMD apt-get install openjdk-8-jdk android-tools-adb bc bison build-essential curl flex g++-multilib gcc-multilib gnupg gperf imagemagick lib32ncurses5-dev lib32readline-dev lib32z1-dev libesd0-dev liblz4-tool libncurses5-dev libsdl1.2-dev libssl-dev libwxgtk3.0-dev libxml2 libxml2-utils lzop pngcrush rsync schedtool squashfs-tools xsltproc yasm zip zlib1g-dev
CMD mkdir -p ~/aosp
CMD cd ~/aosp
CMD repo init -u https://android.googlesource.com/platform/manifest -b android-9.0.0_r45 --depth=1
